#define DIMENSION 300
#define MAX_ITERATIONS 256
#define BAILOUT 2
#define C_REAL .38921
#define C_IMAGINARY .2918
#define N_THREADS 4
//#define C_REAL .62772
//#define C_IMAGINARY .42193

#include <math.h>
#include <iostream>
#include <boost/asio/io_service.hpp>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>
#include <boost/program_options.hpp>

using namespace std;
namespace po = boost::program_options;

void process_pixel(int x, int y, double real_component, double imaginary_component, int dimension);

boost::mutex io_mutex;

int main(int argc, char* argv[]) {
    int thread_count;
    double real_component;
    double imaginary_component;
    int dimension;
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help,h", "print help message")
        ("threads,t", po::value<int>(&thread_count)->default_value(N_THREADS), "number of threads")
        ("real,r", po::value<double>(&real_component)->default_value(C_REAL), "real component")
        ("imaginary,i", po::value<double>(&imaginary_component)->default_value(C_IMAGINARY), "imaginary component")
        ("dimension,d", po::value<int>(&dimension)->default_value(DIMENSION), "size for one side of the square image")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        cout << desc << "\n";
        return 1;
    }
    boost::asio::io_service service;
    boost::thread_group threadpool;
    
    auto_ptr<boost::asio::io_service::work> work(
        new boost::asio::io_service::work(service));
    
    for (int i = 0; i < thread_count; i++) {
        threadpool.create_thread(
            boost::bind(&boost::asio::io_service::run, &service));
    }
   
    
    for (int y = 0; y < dimension; y++) {
        for (int x = 0; x < dimension; x++) {
            service.post(boost::bind(process_pixel, x, y, real_component, imaginary_component, dimension));
        }
    }
    
    //instead of calling service.stop(), call work.reset() to let
    //the unique_ptr be destroyed. Work
    //in progress and queued work will finish. service.stop() preempts jobs
    work.reset();
    threadpool.join_all();
}        

void process_pixel(int x, int y, double real_component, double imaginary_component, int dimension) {

    double z_real_old = 1.5 * (x - dimension / 2) / (.5 * dimension);
    double z_imaginary_old = (y - dimension / 2) / (.5 * dimension);
    int iterations = 0;
    double norm = 0;
    
    while (iterations <= MAX_ITERATIONS && norm <= BAILOUT) {
        double z_real_new = z_real_old * z_real_old - z_imaginary_old * z_imaginary_old + real_component;
        double z_imaginary_new = 2 * z_imaginary_old * z_real_old + imaginary_component;
        
        norm = sqrt(z_real_new * z_real_new + z_imaginary_new * z_imaginary_new);
        
        z_real_old = z_real_new;
        z_imaginary_old = z_imaginary_new;
        iterations++;
    }
    
    io_mutex.lock();
    cout << x << " " << y << " " << iterations << " " << endl;
    io_mutex.unlock();
}
